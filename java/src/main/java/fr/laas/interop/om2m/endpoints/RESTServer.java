package fr.laas.interop.om2m.endpoints;

import java.net.URI;
import javax.ws.rs.core.UriBuilder;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import fr.laas.om2m.client.NotificationListener;

public class RESTServer extends Thread{
	
	private static final Logger LOGGER = LogManager.getLogger(RESTServer.class);
	private String BASE_URL;
	private String name;
	private int port;
	private String url;

	public RESTServer(String ip, String name, int port){
		this.BASE_URL = "http://"+ip;
		this.name = name;
		this.port = port;
		this.url = BASE_URL+":"+port+"/"+name;
	}
	
	public int getListeningPort(){
		return this.port;
	}
	
	public String getURL(){
		return this.url;
	}
	
	public void run() {
		LOGGER.info("Setting up REST server for OM2M client "+name);
		URI baseUri = UriBuilder.fromUri(BASE_URL+"/"+this.name).port(this.port).build();
		ResourceConfig rc = new ResourceConfig()
			.register(NotificationListener.class);
	    HttpServer server = GrizzlyHttpServerFactory.createHttpServer(baseUri, rc);
	    try {
			LOGGER.info("Starting OM2M client REST server");
			server.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
