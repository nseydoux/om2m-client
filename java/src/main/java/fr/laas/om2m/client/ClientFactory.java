package fr.laas.om2m.client;

import java.io.File;
import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.laas.om2m.init.config.OM2MConfiguration;

public class ClientFactory {
	
	private static final Logger LOGGER = LogManager.getLogger(ClientFactory.class);
	private static ClientFactory instance;
	
	private ClientFactory(){}
	
	public static ClientFactory getInstance(){
		if(ClientFactory.instance == null){
			ClientFactory.instance = new ClientFactory();
		}
		return ClientFactory.instance;
	}
	
	public static final String OM2M_INSTANCE = "http://localhost:8080/~";
	public static final String CSE_ID = "/in-cse";
	public static final String CSE_NAME = "/in-name";
	
	private static OM2MConfiguration parseConfig(File configFile){
		ObjectMapper mapper = new ObjectMapper();
		OM2MConfiguration config =null;
		if(configFile != null){
            try {
                config = mapper.readValue(configFile, OM2MConfiguration.class);
            } catch (JsonParseException ex) {
                LOGGER.error("Parsing failed");
                ex.printStackTrace();
            } catch (JsonMappingException e) {
            	LOGGER.error("Mapping exception");
				e.printStackTrace();
			} catch (IOException e) {
				LOGGER.error("IO exception");
				e.printStackTrace();
			}
        }
		return config;
	}
	
	/**
	 * Creates an {@link OM2MClient} instance according to the provided configuration file.
	 * @param configFile
	 * @return the created client
	 */
	public static OM2MClient createClient(File configFile){
		OM2MConfiguration config = parseConfig(configFile);
		OM2MClient client = OM2MClient.getInstance(config.getName(), config.getInstanceIP(), config.getNotificationPort(), config.getInstanceURL(), config.getCseId(), config.getCseName());
		return client;
	}
}
