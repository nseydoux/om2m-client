package fr.laas.om2m.client;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Path("/listener")
public class NotificationListener {
		
	private static final Logger LOGGER = LogManager.getLogger(NotificationListener.class);

	public static final String NOTIF_PATH = "/listener/notify";
	
	@GET
    @Produces(MediaType.TEXT_PLAIN)
	public String hello() {
		return "Notification listener is up.";
	}
	
	@POST
	@Path("/notify")
	@Produces(MediaType.APPLICATION_XML)
	public String receiveNotification(String notification){
		LOGGER.debug("Received notification : "+notification);
		OM2MClient.routeNotification(notification);		
		// The return value is irrelevant, but the method has to return something
		return "<notif>Notification received</notif>";
	}
}
