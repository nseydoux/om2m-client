package fr.laas.om2m.client;


public interface NotificationObserver {
	public void notify(String n);
}
