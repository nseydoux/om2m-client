package fr.laas.om2m.client;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.om2m.commons.constants.MimeMediaType;
import org.eclipse.om2m.commons.constants.ShortName;
import org.eclipse.om2m.commons.obix.Obj;
import org.eclipse.om2m.commons.obix.io.ObixDecoder;
import org.eclipse.om2m.commons.resource.Notification;
import org.eclipse.om2m.datamapping.jaxb.Mapper;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


/**
 * Provide util method(s) for handling the notification from OM2M
 * 
 * @author aissaoui
 *
 */
public class NotificationUtil {
	
	private static final Logger LOGGER = LogManager.getLogger(NotificationUtil.class);

	/**
	 * Returns the linked obix object if any in the notification provided
	 * 
	 * @param notification
	 *            the notification to inspect
	 * @return the oBIX object
	 * @throws NotObixContentException
	 *             if the notification do not contain oBIX object
	 */
	public static final Obj getObixFromNotification(Notification notification)
			throws NotObixContentException {
		if (notification.getNotificationEvent().getRepresentation() instanceof Element) {
			Element element = (Element) notification.getNotificationEvent()
					.getRepresentation();
			Node nodeContent = getContentNode(element);
			if (nodeContent == null) {
				throw new NotObixContentException();
			}
			String contentValue = nodeContent.getTextContent();
			Obj obj = ObixDecoder.fromString(contentValue);
			return obj;
		}
		throw new NotObixContentException();
	}
	
	
	public static final Obj getObixFromXMLNotificationRepresentation(String notification)
			throws NotObixContentException {
		XPath xPath = XPathFactory.newInstance().newXPath();
		DocumentBuilder b;
		try {
			b = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document doc = b.parse(new InputSource(new StringReader(notification)));
			LOGGER.debug(notification);
			LOGGER.debug(doc.toString());
			NodeList nodes = (NodeList)xPath.evaluate("//con", doc.getDocumentElement(), XPathConstants.NODESET);
			LOGGER.debug("Identified "+nodes.getLength()+" content nodes");
			// Only one item is expected
			if(nodes.getLength() > 1){
				throw new NotObixContentException();
			}
			return ObixDecoder.fromString(nodes.item(0).getTextContent());
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		throw new NotObixContentException();
	}

	private static final Node getContentNode(Element element) {
		for (int i = 0; i < element.getChildNodes().getLength(); i++) {
			Node node = element.getChildNodes().item(i);
			if (node.getNodeName().equals(ShortName.CONTENT)) {
				return node;
			}
		}
		return null;
	}

	public static final class NotObixContentException extends Exception {

		private static final long serialVersionUID = 5553296928654621371L;

		public NotObixContentException() {
			super("Content does not contain oBIX representation");
		}
	}

}
