package fr.laas.om2m.client;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.om2m.binding.http.RestHttpClient;
import org.eclipse.om2m.commons.constants.FilterUsage;
import org.eclipse.om2m.commons.constants.MimeMediaType;
import org.eclipse.om2m.commons.constants.NotificationContentType;
import org.eclipse.om2m.commons.constants.Operation;
import org.eclipse.om2m.commons.constants.ResourceType;
import org.eclipse.om2m.commons.constants.ResponseStatusCode;
import org.eclipse.om2m.commons.constants.ResultContent;
import org.eclipse.om2m.commons.resource.AE;
import org.eclipse.om2m.commons.resource.Container;
import org.eclipse.om2m.commons.resource.ContentInstance;
import org.eclipse.om2m.commons.resource.FilterCriteria;
import org.eclipse.om2m.commons.resource.Notification;
import org.eclipse.om2m.commons.resource.RequestPrimitive;
import org.eclipse.om2m.commons.resource.Resource;
import org.eclipse.om2m.commons.resource.ResponsePrimitive;
import org.eclipse.om2m.commons.resource.SemanticDescriptor;
import org.eclipse.om2m.commons.resource.Subscription;
import org.eclipse.om2m.commons.resource.URIList;
import org.eclipse.om2m.commons.resource.Notification.NotificationEvent;
import org.eclipse.om2m.datamapping.jaxb.Mapper;
import org.glassfish.jersey.client.ClientConfig;

import fr.laas.interop.om2m.endpoints.RESTServer;

/**
 * This class should be instanciated using the {@link ClientFactory} class, provided with an 
 * appropriate configuration file. It provides high-level functions to be able to have a 
 * basic use of the OM2M REST API without having to go through the standard.
 */
public class OM2MClient {
	private static final Logger LOGGER = LogManager.getLogger(OM2MClient.class);

	private static final String ORIGINATOR = "admin:admin";

	private static Map<String, OM2MClient> instances;
	private static Map<String, String> notificationRoutingTable;
	
	private String key;
	private String instanceBase;
	private String cseBase = null;
	private String cseName = null;
	private RestHttpClient client;
	private static Mapper mapper;
	
	private RESTServer server;
	private Set<NotificationObserver> observers;
	
	static {
		mapper = new Mapper(MimeMediaType.XML);
		instances = new HashMap<String, OM2MClient>();
		notificationRoutingTable = new HashMap<>();
	}
	
	/**
	 * This function returns an instance of the class, provided that it was previously created by the {@link ClientFactory}.
	 * @param name
	 * @return the instance associated to the specified name, null if there is none.
	 */
	public static OM2MClient getInstance(String name){
		return instances.get(name);
	}
	
	/**
	 * This function is called by the {@link ClientFactory} to spawn the {@link OM2MClient} instances.
	 * @param name
	 * @param instanceBase
	 * @param cseBase
	 * @param cseName
	 * @return
	 */
	protected static OM2MClient getInstance(String name, String ip, int notificationPort, String instanceBase, String cseBase, String cseName) {
		if(instances.get(name) == null){
			instances.put(name, new OM2MClient(name, ip, notificationPort, instanceBase, cseBase, cseName));
		}
		return instances.get(name);
	}
	
	/**
	 * The {@link OM2MClient} class is multiton (named collection of singletons), therefore the constructor is private.
	 * @param key The multiton key
	 * @param notificationPort
	 * @param instanceBase The base URL of the target OM2M server
	 * @param cseBase
	 * @param cseName
	 */
	private OM2MClient(String key, String ip, int notificationPort, String instanceBase, String cseBase, String cseName) {
		this.key = key;
		this.instanceBase = instanceBase;
		this.cseBase = cseBase;
		this.cseName = cseName;
		this.client = new RestHttpClient();
		this.observers = new HashSet<>();
		this.server = new RESTServer(ip, key, notificationPort);
		this.server.run();
	}
	
	/**
	 * Since the NotificationListener resource is a singleton by design (since it is a JAX-RS service),
	 * the {@link OM2MClient} class must route the received notification to its target instance. To do
	 * so, a static routing table is built each time a client subscribes to a resource.
	 * @param notif
	 */
	public static void routeNotification(String notif){
		Notification n = (Notification) mapper.stringToObj(notif);
		for(OM2MClient entry : OM2MClient.instances.values()){
			Subscription s = (Subscription)entry.retrieveResourceFromId(n.getSubscriptionReference(), ResourceType.SUBSCRIPTION);
			if(s != null && notificationRoutingTable.containsKey(s.getResourceID())){
				String clientKey = notificationRoutingTable.get(s.getResourceID());
				instances.get(clientKey).processNotification(notif);
			} else {
				LOGGER.error("No client is associated to this subscription");
			}
		}
	}
	
	/**
	 * Implementation of the pattern observed/observer. Applications can observe the notifications received by the client, 
	 * and process them according to their requirements when notified.
	 * @param obs
	 */
	public void addObserver(NotificationObserver obs){
		this.observers.add(obs);
		LOGGER.debug("Observer added, now "+this.observers.size());
	}
	
	/**
	 * Implementation of the pattern observed/observer. When receiving a notification, the client will 
	 * notify all the observers with the full {@link Notification} instance, so that they can process
	 * it in an applciation-specific manner.
	 * @param notification
	 */
	public void processNotification(String notification) {
		LOGGER.debug("Processing notification : "+notification);
		try {
			Notification n = (Notification) mapper.stringToObj(notification);
			NotificationEvent ne = n.getNotificationEvent();
			// If the notification event is null, it's a notif confirmation
			if (ne != null) {			
				for(NotificationObserver obs : this.observers){
					LOGGER.debug("Notifying an observer");
					obs.notify(notification);
				}
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Low-level function that creates a oneM2M request
	 * @param target URI of the resource that should receive the request
	 * @param content Content of the request
	 * @param op Operation performed by the request (CREATE, RETRIEVE, UPDATE, DELETE)
	 * @param requestContentType Media type of the content
	 * @param rt Resource type of the content
	 * @return The forged request
	 */
	public RequestPrimitive createRP(String target, Object content, BigInteger op, String requestContentType, Integer rt) {
		RequestPrimitive rp = new RequestPrimitive();
		rp.setFrom(OM2MClient.ORIGINATOR);
		rp.setTo(target);
		rp.setContent(content);
		rp.setOperation(op);
		rp.setRequestContentType(requestContentType);
		if(rt != null){
			rp.setResourceType(rt);
		}
		return rp;
	}
	
	/**
	 * Defaults the createRP method to the root of the targeted CSE.
	 * @param content Content of the request
	 * @param op Operation performed by the request (CREATE, RETRIEVE, UPDATE, DELETE)
	 * @param requestContentType Media type of the content
	 * @param rt Resource type of the content
	 * @return The forged request
	 */
	public RequestPrimitive createSimpleRP(Object content, BigInteger op, String requestContentType, Integer rt) {
		return createRP(this.instanceBase+this.cseBase+this.cseName, content, op, requestContentType, rt);
	}
	
	/**
	 * Sends a subscription request to the target. When the target is updated, notifications will be sent 
	 * to the {@link RESTServer} embedded in the {@link OM2MClient}. The subscription resource id is stored
	 * in a static routing table used to route subscription to the instances when it is received by the
	 * class.
	 * @param subscriptionName
	 * @param targetId
	 * @return
	 */
	public Subscription subscribe(String subscriptionName, String targetId) {
		Subscription s = new Subscription();
		s.setName(subscriptionName);
		s.getNotificationURI().add(this.server.getURL()+NotificationListener.NOTIF_PATH);
		s.setNotificationContentType(NotificationContentType.WHOLE_RESOURCE);
		RequestPrimitive rp = createSimpleRP(mapper.objToString(s),
				Operation.CREATE, MimeMediaType.XML, ResourceType.SUBSCRIPTION);
		String to;
		if(targetId.startsWith("/")){
			to = targetId.substring(1);
		} else {
			to = targetId;
		}
		rp.setTo(this.instanceBase + to);
		ResponsePrimitive resp = client.sendRequest(rp);
		if (resp.getResponseStatusCode().equals(ResponseStatusCode.CREATED)) {
			Subscription createdSubs = (Subscription)mapper.stringToObj((String)resp.getContent());
			OM2MClient.notificationRoutingTable.put(createdSubs.getResourceID(), this.key);
			return createdSubs;
		} else {
			LOGGER.error("Subscription failed : "+resp.getResponseStatusCode());
			return null;
		}
	}
	
	/**
	 * Retrieves the resource with the given id. The returned resources representation includes the
	 * attributes as well as the references to the target's child resources.
	 * @param id
	 * @param resourceType
	 * @return The resource with the provided id if any, null otherwise
	 */
	public Resource retrieveResourceFromId(String id, int resourceType) {
		RequestPrimitive rp = createSimpleRP(null, Operation.RETRIEVE,
				MimeMediaType.XML, resourceType);
		rp.setResultContent(ResultContent.ATTRIBUTES_AND_CHILD_REF);
		String to;
		if(id.startsWith("/")){
			to = id.substring(1);
		} else {
			to = id;
		}
		rp.setTo(this.instanceBase + to);
		ResponsePrimitive rsp = client.sendRequest(rp);
		if(rsp.getResponseStatusCode().equals(ResponseStatusCode.OK)){
			return (Resource)mapper.stringToObj((String)rsp.getContent());
		} else {
			System.err.println("Request failed : "+rsp.getResponseStatusCode());
			return null;
		}
	}
	
	/**
	 * Deletes the resource with the given id.
	 * @param id
	 */
	public void deleteResource(String id){
		RequestPrimitive rp = createSimpleRP(null, Operation.DELETE,
				MimeMediaType.XML, null);
		String to;
		if(id.startsWith("/")){
			to = id.substring(1);
		} else {
			to = id;
		}
		rp.setTo(this.instanceBase + to);
		ResponsePrimitive rsp = client.sendRequest(rp);
		if(!rsp.getResponseStatusCode().equals(ResponseStatusCode.DELETED)){
			System.err.println("Request failed : "+rsp.getResponseStatusCode());
		}
	}
	
	/**
	 * Creates a {@link Container} resource under the resource with the target id.
	 * @param name
	 * @param parentId
	 * @return The representation of the created resource, null if the creation failed
	 */
	public Container createContainer(String name, String parentId) {
		Container cnt = new Container();
		cnt.setName(name);

		RequestPrimitive rp = createSimpleRP(mapper.objToString(cnt),
				Operation.CREATE, MimeMediaType.XML, ResourceType.CONTAINER);
		String to;
		if(parentId == null){
			to=this.cseBase+this.cseName;
		} else if(parentId.startsWith("/")){
			to = parentId.substring(1);
		} else {
			to = parentId;
		}
		rp.setTo(this.instanceBase + to);

		ResponsePrimitive resp = client.sendRequest(rp);
		if (resp.getResponseStatusCode().equals(ResponseStatusCode.CREATED)) {
			return (Container) mapper.stringToObj((String) resp.getContent());
		} else {
			LOGGER.error("Creation failed: "
					+ resp.getResponseStatusCode());
			return null;
		}
	}
	
	/**
	 * Creates a {@link ContentInstance} resource under the resource with the target id.
	 * @param name
	 * @param parentId
	 * @return The representation of the created resource, null if the creation failed
	 */
	public ContentInstance createContentInstance(String parentId, String content) {
		ContentInstance cin = new ContentInstance();
		cin.setContent(content);
		RequestPrimitive rp = createSimpleRP(mapper.objToString(cin),
				Operation.CREATE, MimeMediaType.XML, ResourceType.CONTENT_INSTANCE);
		String to;
		if(parentId == null){
			to=this.cseBase+this.cseName;
		} else if(parentId.startsWith("/")){
			to = parentId.substring(1);
		} else {
			to = parentId;
		}
		rp.setTo(this.instanceBase + to);

		ResponsePrimitive resp = client.sendRequest(rp);
		if (resp.getResponseStatusCode().equals(ResponseStatusCode.CREATED)) {
			return (ContentInstance) mapper.stringToObj((String) resp.getContent());
		} else {
			LOGGER.error("Creation failed: "
					+ resp.getResponseStatusCode());
			return null;
		}
	}
	
	/**
	 * Creates an {@link AE} resource under the resource with the target id. The application id 
	 * allows to associate multiple AEs to the same global application.
	 * @param name
	 * @param parentId
	 * @return The representation of the created resource, null if the creation failed
	 */
	public AE createAE(String name, String parentId, String appId) {
		AE ae = new AE();
		ae.setRequestReachability(false);
		ae.setAppID(appId);
		ae.setName(name);

		RequestPrimitive rp = createSimpleRP(mapper.objToString(ae),
				Operation.CREATE, MimeMediaType.XML, ResourceType.AE);
		String to;
		if(parentId == null){
			to=this.cseBase+this.cseName;
		} else if(parentId.startsWith("/")){
			to = parentId.substring(1);
		} else {
			to = parentId;
		}
		rp.setTo(this.instanceBase + to);
		
		ResponsePrimitive resp = client.sendRequest(rp);
		if (resp.getResponseStatusCode().equals(ResponseStatusCode.CREATED)) {
			return (AE) mapper.stringToObj((String) resp.getContent());
		} else {
			LOGGER.error("Creation failed: "
					+ resp.getResponseStatusCode());
			return null;
		}
	}
	
	/**
	 * Creates a {@link SemanticDescriptor} resource under the resource with the target id. The content 
	 * must be well-formed RDF/XML.
	 * @param name
	 * @param parentId
	 * @return The representation of the created resource, null if the creation failed
	 */
	public SemanticDescriptor createSemanticDescriptor(String name, String parentId, String descContent) {
		SemanticDescriptor smd = new SemanticDescriptor();
		smd.setDecodedDescriptor(descContent);
		smd.setDescriptorRepresentation("RDF/XML");
		smd.setName(name);
		RequestPrimitive rp = createSimpleRP(mapper.objToString(smd), Operation.CREATE, MimeMediaType.XML, ResourceType.SEMANTIC_DESCRIPTOR);
		String to;
		if(parentId == null){
			to=this.cseBase+this.cseName;
		} else if(parentId.startsWith("/")){
			to = parentId.substring(1);
		} else {
			to = parentId;
		}
		rp.setTo(this.instanceBase + to);
		ResponsePrimitive resp = client.sendRequest(rp);
		if(resp.getResponseStatusCode().equals(ResponseStatusCode.CREATED)){
			return (SemanticDescriptor) mapper.stringToObj((String)resp.getContent());
		} else {
			LOGGER.error("Creation failed: "+resp.getResponseStatusCode());
			return null;
		}
	}
	
	public List<String> performDiscovery(FilterCriteria fc){
		RequestPrimitive rp = createRP(
				this.instanceBase+this.cseBase+this.cseName,
				null, Operation.RETRIEVE, MimeMediaType.XML,
				ResourceType.AE);
		rp.setFilterCriteria(fc);
		ResponsePrimitive resp = client.sendRequest(rp);
		if(resp.getResponseStatusCode().equals(ResponseStatusCode.OK)){
			return ((URIList)mapper.stringToObj((String)resp.getContent())).getListOfUri();
		} else {
			return null;
		}
	}
	
	/**
	 * Discovers all the resources of a certain type under the CSE.
	 * @param resourceType an integer representing the requested type of resources
	 * @return the URI of the resources of the requested type
	 */
	public List<String> performTypeDiscovery(int resourceType) {
		FilterCriteria fc = new FilterCriteria();
		fc.setFilterUsage(FilterUsage.DISCOVERY_CRITERIA);
		fc.setResourceType(BigInteger.valueOf(resourceType));
		return performDiscovery(fc);
		
	}
	
	/**
	 * Discovers all the resources with certain labels under the CSE.
	 * @param labels list of keywords
	 * @return the URI of the resources having all the keywords as labels
	 */
	public List<String> performLabelDiscovery(List<String> labels) {
		FilterCriteria fc = new FilterCriteria();
		fc.setFilterUsage(FilterUsage.DISCOVERY_CRITERIA);
		fc.getLabels().addAll(labels);
		return performDiscovery(fc);
	}
	
	/**
	 * Sends the provided SPARQL query to the root of the CSE.
	 * @param query A SPARQL query
	 * @return the URI of the resources having a {@link SemanticDescriptor} matching the query
	 */
	public List<String> performSemanticDiscovery(String query) {
		FilterCriteria fc = new FilterCriteria();
		fc.setFilterUsage(FilterUsage.DISCOVERY_CRITERIA);
		List<String> semanticFilterList = new ArrayList<String>();
		semanticFilterList.add(query);	
		fc.setSemanticFilter(semanticFilterList);
		return performDiscovery(fc);
	}
	
	/**
	 * 
	 * @param resourcePath relative to the cse name (hierarchical only)
	 * @param queryStrings
	 */
	public void postCommand(String resourcePath, Map<String, String> queryStrings){
		Client client = ClientBuilder.newClient(new ClientConfig());
		WebTarget wt = client.target(this.instanceBase+this.cseBase+this.cseName+resourcePath);
		for(Entry<String, String> queryParam : queryStrings.entrySet()){
			wt = wt.queryParam(queryParam.getKey(), queryParam.getValue());
		}
		wt.request().header("X-M2M-ORIGIN", ORIGINATOR).post(Entity.entity("", "text/plain"));
	}
}
