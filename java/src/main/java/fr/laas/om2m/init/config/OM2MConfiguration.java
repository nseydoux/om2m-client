package fr.laas.om2m.init.config;

public class OM2MConfiguration {
	private String name;
	private String instanceURL;
	private String cseId;
	private String cseName;
	private int notificationPort;
	private String instanceIP;
	
	public String getInstanceURL() {
		return instanceURL;
	}
	public void setInstanceURL(String instanceURL) {
		this.instanceURL = instanceURL;
	}
	public String getCseId() {
		return cseId;
	}
	public void setCseId(String cseId) {
		this.cseId = cseId;
	}
	public String getCseName() {
		return cseName;
	}
	public void setCseName(String cseName) {
		this.cseName = cseName;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getNotificationPort() {
		return notificationPort;
	}
	public void setNotificationPort(int notificationPort) {
		this.notificationPort = notificationPort;
	}
	public String getInstanceIP() {
		return instanceIP;
	}
	public void setInstanceIP(String instanceIP) {
		this.instanceIP = instanceIP;
	}
	

}
