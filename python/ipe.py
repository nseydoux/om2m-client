from om2m_client import OM2MClient
from sensors import Barometer, Anemometer, TemperatureSensor, LightSensor
from om2m_resources.resource import cse, ae, cnt, cin, sub
import om2m_resources.constants as om2m_cst
import json


class Weather_Station():
    def __init__(self):
        self.barometer = Barometer("MyBarometer")
        self.anemometer= Anemometer("MyAnemometer")
        self.temperature= TemperatureSensor("MyThermometer")

class Room():
    def __init__(self, name):
        self.name = name
        self.temperature=tempereature.TemperatureSensor(name+"Temperature")
        self.luminosity = luminosity.LightSensor(name+"Light")

class ADREAM_IPE():
    def __init__(self):
        self.weather = Weather_Station()
        self.rooms = []
        for room in ["H101, H102, H103"]:
            self.rooms.append(Room(room))
        self.client = OM2MClient("http://localhost:8080/~/", "in-cse/", 4567)
        self.weather_ae = self.client.create_ae("in-cse/in-name/", "Weather_Station", "Adream control")
        self.weather_ae_name = "in-cse/in-name/Weather_Station"
        self.initialize_weather(self.weather_ae_name)

    def initialize_weather(self, ae_id):
        # The anemometer
        self.client.create_cnt(ae_id, "Anemometer")
        self.client.create_cnt(ae_id, "Thermometer")
        self.client.create_cnt(ae_id, "Barometer")

    def read_values(self):
        self.client.create_cin(self.weather_ae_name+"/Barometer", self.weather.barometer.read_Value())
        self.client.create_cin(self.weather_ae_name+"/Thermometer", self.weather.temperature.read())
        self.client.create_cin(self.weather_ae_name+"/Anemometer", self.weather.anemometer.value())
