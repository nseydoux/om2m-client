import requests
import json
from om2m_resources.resource import cse, ae, cnt, cin, sub, sgn, smd
from om2m_resources.protocol import request_primitive
import om2m_resources.constants as constants
from http.server import BaseHTTPRequestHandler, HTTPServer
from threading import Thread
import cherrypy

@cherrypy.expose
class Listener(object):
    def __init__(self, client):
        self.client = client

    def GET(self):
        return "OM2M client on"

    def POST(self, length=8):
        body = cherrypy.request.body.read()
        print("Received a notification")
        notif = sgn(body.decode("utf-8"))
        if "sur" in notif.__dict__:
            subscription=sub(json_obj=self.client.retrieve_resource(notif.sur[1:]))
            self.client.listening_callback[subscription.ri](notif)
        return "POST response"

class OM2MClient():
    def __init__(self, url, cse_id, port):
        self.url = url
        self.cse_id = cse_id
        self.originator = "admin:admin"
        self.port = port
        self.listener = Thread(target=self.listen)
        self.listener.start()
        self.listening_endpoint = "http://localhost:"+str(self.port)
        self.listening_callback = {}

    def create_resource(self, parent, resource):
        r = request_primitive(originator="admin:admin", to=self.url+parent, content=resource, operation=constants.CREATE)
        response = r.execute()
        if response.status_code != 201:
            print("Creation failure "+str(response.status_code)+" : "+response.text)
            return None
        else:
            return response

    def retrieve_resource(self, resource_id):
        r = request_primitive(originator="admin:admin", to=self.url+resource_id, content=None, operation=constants.RETRIEVE)
        response = r.execute()
        if response.status_code == 200:
            return response.text
        else:
            return None

    def exists(self, resource):
        return self.retrieve_resource(resource.ri[1:]) != None

    def create_ae(self, parent, name, app_id):
        my_ae = ae()
        my_ae.rn = name
        my_ae.api = app_id
        my_ae.rr = "false"
        response = self.create_resource(parent, my_ae)
        if response != None:
            return ae(json_obj=response.text)
        else:
            print("Creation failed")
            return None

    def create_cnt(self, parent, name):
        my_cnt = cnt()
        my_cnt.rn = name
        response = self.create_resource(parent, my_cnt)
        if response != None:
            return cnt(json_obj=response.text)
        else:
            print("Creation failed")
            return None

    def create_cin(self, parent, content):
        my_cin = cin()
        my_cin.con = content
        response = self.create_resource(parent, my_cin)
        if response != None:
            return cin(json_obj=response.text)
        else:
            print("Creation failed")
            return None

    def create_smd(self, parent, content, name=None, rels=None):
        my_smd = smd()
        if name:
            my_smd.rn = name
        if rels:
            my_smd.rels = rels
        my_smd.dsp = content
        my_smd.dcrp = "RDF/XML"
        response = self.create_resource(parent, my_smd)
        if response != None:
            return smd(json_obj=response.text)
        else:
            print("Creation failed")
            return None

    def subscribe(self, target, name, callback):
        my_subs = sub()
        my_subs.rn = name
        # notification url
        my_subs.nu = self.listening_endpoint
        # notification content type
        my_subs.nct = constants.WHOLE_RESOURCE
        #self.listening_callback = callback
        response = self.create_resource(target, my_subs)
        print(response.text)
        if response != None:
            subscription = sub(json_obj=response.text)
            self.listening_callback[subscription.ri] = callback
            return subscription
        else:
            print("Creation failed")
            return None

    @staticmethod
    def extract_from_notification(constructor, short_name, nev):
        resource = {}
        resource["m2m:"+short_name] = nev["rep"]
        json_rep = json.dumps(resource)
        return constructor(json_obj=json_rep)

    def listen(self):
        print('starting server...')
        conf = {
            '/': {
                'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
                'tools.sessions.on': True,
                'tools.response_headers.on': True,
                'tools.response_headers.headers': [('Content-Type', 'text/plain')],
            }
        }
        cherrypy.config.update({'server.socket_port': self.port})
        cherrypy.quickstart(Listener(self), "/", conf)

    def wait_forever(self):
        self.listener.join()
